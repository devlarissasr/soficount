import React from 'react';
import { Container, Header, Left, Body, Right, Title, Content, Button, Text, FooterTab,  Footer, Icon, Card, CardItem, Badge  } from 'native-base';
import { StyleSheet } from 'react-native';



const styles = StyleSheet.create({
    sizeButton: {
        marginTop: 10,
        marginLeft: 150,
    },
    sizeBadge: {
        width: 180,
        height:180,
        marginLeft:100,
        marginTop: 60,
        borderColor: '#ff4266',
        borderRadius: 4,
        backgroundColor: '#ff4266',
    },
    textBadge: {
        fontSize:150,
        marginLeft: 35,



    },
    cardText: {
        alignItems: 'center',
        justifyContent: 'center',

    },
    cardStyle:{
        width: 350,
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 40,
        marginLeft: 12,
        borderColor: '#ff4266',

    }

});

export default function Home() {
    return (
        <Container>
            <Header>
                <Left/>
                <Body>
                    <Title>SofiCount</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                <Card  style={styles.cardStyle}>
                    <CardItem header>
                        <Text>Como funciona?</Text>
                    </CardItem>
                    <CardItem>
                        <Body style={styles.cardText}>
                            <Text>
                                Todas as vezes que a Sofia disser "kkk" clique no botão para somar +1. A cada 50 = 1 encontro.
                            </Text>
                        </Body>
                    </CardItem>
                </Card>
                <Card  style={styles.sizeBadge}>
                <CardItem>
                    <Body>
                        <Text style={styles.textBadge}>
                           0
                        </Text>
                    </Body>
                </CardItem>
                </Card>
                <Button dark style={styles.sizeButton}><Text> Para +1  </Text></Button>
            </Content>
            <Footer>
                <FooterTab>
                    <Button vertical>
                        <Icon name="apps" />
                        <Text>Relatório</Text>
                    </Button>
                    <Button badge vertical>
                        <Badge><Text>3</Text></Badge>
                        <Icon name="person" />
                        <Text>Encontros</Text>
                    </Button>
                </FooterTab>
            </Footer>
        </Container>
    );
}
